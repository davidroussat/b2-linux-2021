# Clés SSH

## Génération de clés SSH

A effectuer **sur le poste d'administration** (sur votre poste à vous, votre PC. **PAS LA VM**) :

```bash
# Génération de la paire de clé
# L'algo de chiffrement utilisé est RSA, la clé a une longueur de 4096 bits
$ ssh-keygen -t rsa -b 4096
```

Une fois la commande tapée, on vous demandera deux infos :
* le chemin où enregitrer les clés
  * je vous conseille de laisser par défaut, sinon vous devrez le préciser explicitement sur toutes vos commandes/configurations
* un mot de passe pour protéger la clé
  * empêche la clé d'être utilisable sans un mot de passe

## Déposer la clé publique à distance

La clé **publique** de la paire de clés générée précédemment doit être déposée à distance, sur la machine où l'on souhaite se connecter.

> On suppose pour la suite que la clé privée est enregistrée dans `/home/user/.ssh/id_rsa` et la clé publique dans `/home/user/.ssh/id_rsa.pub`.

Récupérer la clé publique **sur la machine hôte** :
```bash
# On récupère le contenu de la clé publique
$ cat /home/user/.ssh/id_rsa.pub
```

---

Le contenu de ce fichier `/home/user/.ssh/id_rsa.pub` doit être ajouté à un fichier sur la machine virtuelle pour permettre la connexion à distance.

Il faut donc créer un fichier `/home/user/.ssh/authorized_keys` **sur la VM** :
```bash
# On se déplace dans notre répertoire personnel
$ cd

# Création d'un répertoire .ssh
$ mkdir .ssh
$ cd .ssh

# Création d'un fichier, puis y coller la clé qu'on a généré plus haut
$ vim authorized_keys

# Exemple de contenu du fichier `/home/user/.ssh/authorized_keys`
$ cat /home/user/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQClIbguv1tMXre8prPRL+0CsFW9EGGbQVt4uzhFE55TtOcGgRvoK6Kea0LQai8SY+ppFA8MFzh+xHOxwJOUYq28+UWP7wOm1NPdcD2LjfCZLDj2sApBRSwBw1ZRMhzOMxEodGlppFhS4f3NWfhizJTyz/IhE5VICq/h48ezftFvpMJvWYiJ8C/TcdwGSF8BUvWJZhxj6qSQ7KsXBh2bR2rxxqZSKSVTmukC8zM5CpNYV5dKALtlg1b7w94PmxOX7ipmXgrsh65NhNDAryK1C6hvJIp8xyvKAgE8HN+7EBF02XgG0r5F1F06/zhVkYyUfafHF12j1d7RU2QIUSnRnZM3lYTFw0F/h2k+d/XsPd929Fk0i+cqhuViNUkxtiLqCdLuMkaNvN8pru68tgX7ofeatT4UbfegrFRdRYGSUBF/Y8hvgfpYzKnzpMJtGwM8vk7kb8NpUg7u/KZz0HwR/ExtL5PJwHncEH0KJowisZ72oZeoA2XE9+QmA0C0+ZA0ZdTRF8WGxFx4DnmeLQcu+ZVO2J8nei8eXuexohezAGMcy4pFxkCpyFaeBXMyT1SNVMYTMQDdAIeFgb/bAZ+IS18p5uA6KyzrA0oZfoxgZ1KvT6/4f99reAUc4n1amvBXEbM9nh/WIDZGAKylmPcxE7Yne0Tf2YvVDZee7LvKJfgJpw==
```

Définir les permissions sur le dossier `.ssh` et le fichier `authorized_keys` :
```bash
$ chmod 600 /home/user/.ssh/authorized_keys
$ chmod 700 /home/user/.ssh
```

## Test

Depuis la machine hôte, re-tester une connexion SSH, aucun mot de passe ne devrait vous être demandé. 

