# Cours Linux B2

Cours :
- [FHS : File Hierarchy Standard](./fhs/)
- [Partitionnement](./part/)
- [Clés SSH](./ssh/)

Mémos :
- [Mémo commandes](./memo/commandes.md)
- [Mémo réseau Rocky Linux](./memo/rocky_network.md)
- [Etapes installation de la VM patron Rocky Linux](./memo/install_vm.md)
